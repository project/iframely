<?php

/**
 * @file
 * iframely admin configuration form.
 */

/**
 * Admin form to configure the iframely provider.
 */
function iframely_admin() {
  $form = array();

  $form['iframely_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('iframely API Key'),
    '#default_value' => variable_get('iframely_api_key', NULL),
    '#description' => t('iframey requires developers to authenticate their requests to all providers, except "iframe.ly/*". You must <a href="@pricing">sign up </a> to receive a key.', array('@pricing' => 'https://iframely.com/signup')),
  );

  return system_settings_form($form);
}
