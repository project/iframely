<?php

/**
 * @file
 * iframely integration with oEmbed.module.
 */

/**
 * Implement hook_ctools_plugin_directory().
 */
function iframely_ctools_plugin_directory($module, $plugin) {
  if ($module == 'oembed' && $plugin == 'providers') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_menu().
 */
function iframely_menu() {
  $items = array();

  $items['admin/config/media/oembed/provider/iframely'] = array(
    'title' => 'iframely',
    'description' => 'Settings for the iframely provider.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('iframely_admin'),
    'file' => 'iframely.admin.inc',
    'type' => MENU_LOCAL_TASK,
    'access arguments' => array('administer oembed'),
  );

  return $items;
}

/**
 * Implementation of iframely_providers().
 *
 * Fetches all available providers, provided by iframely and caches them in
 * order to check for matches when processing.
 *
 * Since the current implementation of oembed.module merges all data into one
 * string to check via preg_metch, we don't use this function for now, because
 * the string gets to big for php to handle with over 1800 available domains.
 */
function iframely_providers() {
  // Return already cached values.
  $cache = cache_get('iframely');
  if ($cache && isset($cache->data)) {
    return $cache->data;
  }

  // Get domains from iframely.
  $response = drupal_http_request('http://iframe.ly/domains.json');
  $json = ($response->code == 200) ? json_decode($response->data): FALSE;
  cache_set('iframely', $json, 'cache', REQUEST_TIME + 3600 * ($json ? 24 : 6));

  return $json;
}
