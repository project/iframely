<?php

/**
 * @file
 * iframely provider plugin.
 */

$plugin = array(
  'title' => 'iframely',
  'callback' => 'iframely_provider_callback',
  // Since the scheme is to big for preg_match to handle, and iframely handles
  // every page anyway, we skip scheme check.
  'scheme' => FALSE,
  'endpoint' => 'http://iframe.ly/api/oembed',
  'consumer' => TRUE,
  // Assure that iframely is processed as last plugin, since it will always
  // match, to give other plugins the possibility to get in action.
  'weight' => 999999
);

/**
 * Implementation of iframely_provider_callback().
 *
 * Call the api to generate a response for a given, valid url.
 */
function iframely_provider_callback($plugin, $url, $matches, $parameters) {
  // Set needed parameters.
  $parameters['api_key'] = variable_get('iframely_api_key', NULL);
  $parameters['url'] = $url;

  // Load the default callback, since it already handles all we need.
  $default_plugin = ctools_get_plugins('oembed', 'providers', 'default');
  $function = ctools_plugin_get_function($default_plugin, 'callback');

  return $function($plugin, $url, $matches, $parameters);
}
